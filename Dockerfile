# Code based from https://github.com/innerspacehq/docker-pcl/blob/master/Dockerfile
FROM ubuntu as pcl
MAINTAINER nips6828@gmail.com

# install prereqs
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-get update
RUN apt-get install -y git build-essential linux-libc-dev
RUN apt-get install -y cmake cmake-gui 
RUN apt-get install -y libusb-1.0-0-dev libusb-dev libudev-dev
RUN apt-get install -y mpi-default-dev openmpi-bin openmpi-common  
RUN apt-get install -y libflann-dev
RUN apt-get install -y libeigen3-dev
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y libvtk7-qt-dev
RUN apt-get install -y libqhull* libgtest-dev
RUN apt-get install -y freeglut3-dev pkg-config
RUN apt-get install -y libxmu-dev libxi-dev 
RUN apt-get install -y mono-complete
run apt-get update -y && apt-get install -y libboost-all-dev qttools5-dev-tools qt5-default cmake graphviz vim

# get pcl
#RUN add-apt-repository -y ppa:v-launchpad-jochen-sprickerhof-de/pcl
#RUN apt-get update
RUN git clone https://github.com/PointCloudLibrary/pcl.git ~/pcl
WORKDIR /root/pcl/cmake
COPY broadwell.patch . 
run patch < broadwell.patch
run rm broadwell.patch

# install pcl
RUN mkdir ~/pcl/release
RUN cd ~/pcl/release && cmake -DCMAKE_BUILD_TYPE=None -D -DCMAKE_INSTALL_PREFIX=/usr \
           -DBUILD_GPU=ON -DBUILD_apps=ON -DBUILD_examples=ON \
           -DCMAKE_INSTALL_PREFIX=/pcl/  ~/pcl
RUN cd ~/pcl/release && make -j $(nproc)
RUN cd ~/pcl/release && make -j $(nproc) install

from scratch 
Maintainer nips6828@gmail.com
workdir /
copy --from=pcl /pcl /
